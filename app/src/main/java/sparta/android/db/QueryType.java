package sparta.android.db;

import sparta.android.db.realm.RealmManagerUtils;

/**
 * Created by android on 6/1/18.
 * <p>Database query executor tha is responsible to execute transaction based on user request</p>
 */

public enum QueryType {

    GET(){
        @Override
        Object execute(DbParam request) {
            return RealmManagerUtils.query(request);
        }
    },
    INSERT(){
        @Override
        Object execute(DbParam request) {
            RealmManagerUtils.insert(request);
            return null;
        }
    },
    INSERT_OR_UPDATE(){
        @Override
        Object execute(DbParam request) {
            RealmManagerUtils.insertOrUpdate(request);
            return null;
        }
    },
    GET_ASYNC(){
        @Override
        Object execute(DbParam request) {
            return RealmManagerUtils.query(request);
        }
    },
    INSERT_ASYNC(){
        @Override
        Object execute(DbParam request) {
            RealmManagerUtils.insertAsync(request);
            return null;
        }
    },
    INSERT_OR_UPDATE_ASYNC(){
        @Override
        Object execute(DbParam request) {
            RealmManagerUtils.insertOrUpdateAsync(request);
            return null;
        }
    },
    DELETE(){
        @Override
        Object execute(DbParam request) {
            return RealmManagerUtils.delete(request);
        }
    },
    DELETE_ALL(){
        @Override
        Object execute(DbParam request) {
            return RealmManagerUtils.deleteAll(request);
        }
    };

    /**
     * override method for the requested transaction query
     *
     * @param request {@link DbParam}
     * @return object such as string/data model
     */
    abstract Object execute(DbParam request);
}
