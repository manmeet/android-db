package sparta.android.db.demo;

import android.app.Application;

import io.realm.Realm;
import sparta.android.db.DbConfiguration;

/**
 * Created by android on 8/1/18.
 */

public class DbApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        new DbConfiguration.Builder()
                .databaseName("test-db")
                .databaseVersion(1)
                .build();
    }
}
