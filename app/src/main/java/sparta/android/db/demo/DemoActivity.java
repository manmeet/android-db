package sparta.android.db.demo;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;
import java.util.Random;

import sparta.android.db.DbCallbackHandler;
import sparta.android.db.DbResponseParam;
import sparta.android.db.QueryBuilder;
import sparta.android.db.QueryType;
import sparta.android.db.R;
import sparta.android.db.ui.DbConnect;

public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        insertOrupdateData();
    }


    /**
     * Functionality to insert values
     */
    private void insertIntoDb() {
        Angel angel = new Angel();
        angel.setName("name: " + new Random().nextInt());
        DbConnect.operation(this, QueryType.INSERT, Angel.class, angel)
                .callback(new DbCallbackHandler().new DbCallback() {
                    @Override
                    public void onDbSuccess(Object dbModel, DbResponseParam response) {
                        super.onDbSuccess(dbModel, response);
                        Log.d(getClass().getSimpleName(), "onDbSuccess() -> "
                                + dbModel + " message " + response.getMessage()
                                + " taskId " + response.getTaskId()
                                + " response " + response.getResponse());
                        //fetch data from db
                        getDataFromDb();
                    }
                })
                .execute();
    }


    /*Functionality to get stored data from db*/
    private void getDataFromDb() {
        Object object = DbConnect.operation(this, QueryType.GET, Angel.class, null)
                .callback(new DbCallbackHandler.OnDbCallback() {
                    @Override
                    public void onDbSuccess(Object dbModel, DbResponseParam response) {
                        Log.d(getClass().getSimpleName(), "onDbSuccess() -> "
                                + dbModel + " message " + response.getMessage()
                                + " taskId " + response.getTaskId()
                                + " response " + response.getResponse());
                    }

                    @Override
                    public void onDbError(Object dbModel, DbResponseParam response) {
                        Log.d(getClass().getSimpleName(), "onDbError() -> "
                                + dbModel + " message " + response.getMessage()
                                + " taskId " + response.getTaskId()
                                + " response " + response.getResponse());
                    }

                    @Override
                    public void onDbProgress(int taskId, boolean isCompleted) {
                        Log.d(getClass().getSimpleName(), "onDbProgress() -> "
                                + " completed " + isCompleted
                                + " taskId " + taskId);
                    }
                })
                .execute();
        //print logs of result
        if (object instanceof List<?>) {
            List<Angel> results = (List) object;
            if (!results.isEmpty()) {
                for (Angel angel : results) {
                    Log.d(getClass().getSimpleName(), angel.getName());
                }

            }
        }
    }

    /**
     * Functionality to update data
     *
     */
    private void insertOrupdateData() {
        Angel angel = new Angel();
        angel.setName("name: update - " + new Random().nextInt());
        DbConnect.operation(this, QueryType.INSERT_OR_UPDATE, Angel.class, angel)
                .callback(new DbCallbackHandler().new DbCallback() {
                    @Override
                    public void onDbSuccess(Object dbModel, DbResponseParam response) {
                        super.onDbSuccess(dbModel, response);
                        Log.d(getClass().getSimpleName(), "onDbSuccess() -> "
                                + dbModel + " message " + response.getMessage()
                                + " taskId " + response.getTaskId()
                                + " response " + response.getResponse());
                        getDataFromDb();
                        insertIntoDb();

                    }

                    @Override
                    public void onDbError(Object dbModel, DbResponseParam response) {
                        Log.d(getClass().getSimpleName(), "onDbError() -> "
                                + dbModel + " message " + response.getMessage()
                                + " taskId " + response.getTaskId()
                                + " response " + response.getResponse());
                    }
                })
                .execute();
    }

}
