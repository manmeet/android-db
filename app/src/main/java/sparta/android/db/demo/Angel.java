package sparta.android.db.demo;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by android on 9/1/18.
 * pojo class for testing purposes
 */

@RealmClass
public class Angel implements RealmModel {
    @PrimaryKey
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
