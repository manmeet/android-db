package sparta.android.db;

import android.text.TextUtils;
import android.widget.ProgressBar;

import sparta.android.db.DbParam;
import sparta.android.db.R;
import sparta.android.db.ui.ProgressViewApplication;


/**
 * Created by android on 6/1/18.
 * <p>Contains the utility/helper methods for database transaction query request</p>
 */

public class DbUtility {

    /**Constructor definition*/
    private DbUtility(){}

    /**
     * Resolve progress dialog if available otherwise create new here
     *
     * @param param the database param
     * @return {@link ProgressBar}
     */
    public static ProgressBar resolveProgressDialog(DbParam param) {
        if (param.progressBar == null) {
            ProgressViewApplication progressApp = new ProgressViewApplication(param.getContext());
            ProgressBar progressDialog = progressApp.getProgressBar();
            progressApp.setProgressText(TextUtils.isEmpty(param.progressMessage)
                    ? param.context.getString(R.string.loading) : param.progressMessage);
            progressApp.setCancelableOutside(false);
            param.progressBar = progressDialog;
        }
        return param.progressBar;
    }
}
