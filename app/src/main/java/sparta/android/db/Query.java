package sparta.android.db;

import java.util.Date;

/**
 * Created by android on 5/1/18.
 * <p>Query parameter class that contains the parameter to create a query for db</p>
 */

public class Query {
    /*class that are being used as database table*/
    Class<?> dbClass;
    /**
     * compare equality
     */
    String equalToKey;
    String equalToValue;
    /**
     * compare unequality
     */
    String notEqualToKey;
    String notEqualToValue;
    /**
     * compare less than
     */
    String lessThanKey;
    int lessThanValue;
    /**
     * compare less than equal
     */
    String lessThanOrEqualToKey;
    int lessThanOrEqualToValue;
    /**
     * compare greater than date
     */
    String greaterThanKey;
    int greaterThanValue;
    /**
     * compare greater than equal
     */
    String greaterThanOrEqualToKey;
    int greaterThanOrEqualToValue;
    /**
     * The Order by.
     */
    String orderBy;
    /**
     * The Grp by.
     */
    String grpBy;
    /**
     * The Having.
     */
    String having;
    /**
     * The Limit.
     */
    int limit;
    /**
     * The distinct.
     */
    String distinct;
    /**
     * Range {@link Date}
     */
    String betweenDateKey;
    Date betweenDateFrom;
    Date betweenDateTo;

    /********************
     **Getters*
     * ******************/
    public Class<?> getDbClass() {
        return dbClass;
    }

    public String getEqualToKey() {
        return equalToKey;
    }

    public String getEqualToValue() {
        return equalToValue;
    }

    public String getNotEqualToKey() {
        return notEqualToKey;
    }

    public String getNotEqualToValue() {
        return notEqualToValue;
    }

    public String getLessThanKey() {
        return lessThanKey;
    }

    public String getLessThanOrEqualToKey() {
        return lessThanOrEqualToKey;
    }

    public String getGreaterThanKey() {
        return greaterThanKey;
    }

    public String getGreaterThanOrEqualToKey() {
        return greaterThanOrEqualToKey;
    }

    public int getLessThanValue() {
        return lessThanValue;
    }

    public int getLessThanOrEqualToValue() {
        return lessThanOrEqualToValue;
    }

    public int getGreaterThanValue() {
        return greaterThanValue;
    }

    public int getGreaterThanOrEqualToValue() {
        return greaterThanOrEqualToValue;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getGrpBy() {
        return grpBy;
    }

    public String getHaving() {
        return having;
    }

    public int getLimit() {
        return limit;
    }

    public String getDistinct() {
        return distinct;
    }

    public String getBetweenDateKey() {
        return betweenDateKey;
    }

    public Date getBetweenDateFrom() {
        return betweenDateFrom;
    }

    public Date getBetweenDateTo() {
        return betweenDateTo;
    }
}
