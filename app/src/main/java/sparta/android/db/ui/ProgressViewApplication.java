package sparta.android.db.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import sparta.android.db.R;

/**
 * Created by clickapps on 3/7/15.
 */
public class ProgressViewApplication extends RelativeLayout {
    private TextView textView;
    private ProgressBar progressBar;
    private View view;

    /*To display message with progress bar*/
    private String progressMessage;
    /*To identify cancelable for out side touch*/
    private boolean isCancelableOutside = true;

    /**
     * Instantiates a new Progress view application.
     *
     * @param context the context
     */
    public ProgressViewApplication(Context context) {
        super(context);
        init(null);
    }

    /**
     * Instantiates a new Progress view application.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public ProgressViewApplication(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    /**
     * Instantiates a new Progress view application.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    public ProgressViewApplication(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);

    }

    private void init(AttributeSet attrs) {
        view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_progress_base, null);
        textView = (TextView) view.findViewById(R.id.progressViewApplication_textView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressViewApplication_progressBar);
        if (attrs != null) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs,
                    R.styleable.ProgressViewApplication);
            // intermediateDrawable
            int intermediateDrawable = ta.getResourceId(R.styleable.ProgressViewApplication_indeterminateDrawable, -1);
            if (intermediateDrawable != -1)
                progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(getContext(), intermediateDrawable));
            // intermediateDrawable color
//            int tint = ta.getResourceId(R.styleable.ProgressViewApplication_android_tint, -1);
//            if (tint != -1)
//                progressBar.setIndeterminateTint(tint);
            // ProgressText
            String progressText = ta.getString(R.styleable.ProgressViewApplication_android_text);
            if (!TextUtils.isEmpty(progressText)) {
                textView.setVisibility(VISIBLE);
                textView.setText(progressText);
            } else {
                textView.setText(" ");
                textView.setVisibility(GONE);
            }
            // textColor
            int textColor = ta.getResourceId(R.styleable.ProgressViewApplication_android_textColor, -1);
            if (textColor != -1)
                textView.setTextColor(ContextCompat.getColor(getContext(), textColor));
            ta.recycle();
            addView(view);
        }
    }


    /**
     * Gets text view.
     *
     * @return the text view
     */
    public TextView getTextView() {
        return textView;
    }

    /**
     * Gets progress bar.
     *
     * @return the progress bar
     */
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public String getProgressMessage() {
        return progressMessage;
    }

    public boolean isCancelableOutside() {
        return isCancelableOutside;
    }

    /**
     * Setters
     */
    public void setCancelableOutside(boolean isCancelable) {
        this.isCancelableOutside = isCancelable;
    }


    /**
     * Enable white color progress bar.
     */
    public void enableWhiteColorProgressBar() {
        if (getProgressBar() != null) {
            getProgressBar().getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFFFFF));
        }
    }

    /**
     * Sets progress text.
     *
     * @param resId the res id
     */
    public void setProgressText(@StringRes int resId) {
        getTextView().setVisibility(VISIBLE);
        getTextView().setText(resId);
    }

    /**
     * Sets progress text.
     *
     * @param text the text
     */
    public void setProgressText(String text) {
        if (!TextUtils.isEmpty(text)) {
            this.progressMessage = text;
            getTextView().setVisibility(VISIBLE);
            getTextView().setText(text);
        }
    }

    /**
     * Enable white color progress bar.
     *
     * @param color in #RRGGBB
     */
    public void setColorProgressBar(int color) {
        if (getProgressBar() != null) {
            getProgressBar().getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFF000000, color));
        }
    }


}
