package sparta.android.db.ui;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import io.realm.RealmModel;
import sparta.android.db.DbBuilder;
import sparta.android.db.Query;
import sparta.android.db.QueryType;

/**
 * Created by android on 9/1/18.
 * <p>Initial class to create builder and its database query</p>
 */

public class DbConnect {
    /**
     * private constructor
     */
    private DbConnect() {
    }

    /**
     * With builder.
     *
     * @param context         the context
     * @param queryType       {@link QueryType}
     * @param dbClass         class to query
     * @param queryDataObject database class that need to query from
     * @return the builder
     */
    public static <T extends RealmModel> DbBuilder operation(@NonNull Activity context, @NonNull QueryType queryType,
                                                             Class<?> dbClass, @NonNull T queryDataObject) {
        return new DbBuilder(context, queryType, dbClass, queryDataObject);
    }

    /**
     * With builder.
     *
     * @param context         the context
     * @param queryType       {@link QueryType}
     * @param dbClass         class to query
     * @param queryDataObject database class that need to query from
     * @return the builder
     */
    public static <T extends RealmModel> DbBuilder operation(@NonNull Context context, @NonNull QueryType queryType,
                                                             Class<?> dbClass, @NonNull T queryDataObject) {
        return new DbBuilder(context, queryType, dbClass, queryDataObject);
    }
}
