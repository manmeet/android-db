package sparta.android.db;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ProgressBar;

import io.realm.RealmModel;

/**
 * Created by android on 5/1/18.
 * <p>builder to create db transaction query with callback & execute</p>
 */

public class DbBuilder<T extends RealmModel> {
    /*Instance variables*/
    private DbParam param;

    /**
     * Constructor definition
     *
     * @param queryType     {@link QueryType}
     * @param dbClass       to perform db query on
     * @param dbClassObject database class that need to query from
     */
    public DbBuilder(@NonNull QueryType queryType, @NonNull Class<?> dbClass,
                     @NonNull T dbClassObject) {
        param = new DbParam();
        getParam().queryType = queryType;
        getParam().dbClass = dbClass;
        getParam().queryDataObject = dbClassObject;
    }

    /**
     * Constructor definition
     *
     * @param context
     * @param queryType     {@link QueryType}
     * @param dbClass       to perform db query on
     * @param dbClassObject database class that need to query from
     */
    public DbBuilder(@NonNull Context context, @NonNull QueryType queryType,
                     @NonNull Class<?> dbClass, @NonNull T dbClassObject) {
        param = new DbParam();
        getParam().context = context;
        getParam().queryType = queryType;
        getParam().dbClass = dbClass;
        getParam().queryDataObject = dbClassObject;
    }

    /**
     * apply callback for this query request
     *
     * @param callback {@link DbCallbackHandler}
     * @return the builder
     */
    public DbBuilder callback(DbCallbackHandler.OnDbCallback callback) {
        getParam().callback = callback;
        return this;
    }

    /**
     * Task id builder.
     *
     * @param taskId the task id
     * @return the builder
     */
    public DbBuilder taskId(int taskId) {
        getParam().taskId = taskId;
        return this;
    }

    /**
     * Progress dialog builder.
     *
     * @param progressDialog the progress dialog
     * @param message        the message
     * @return the builder
     */
    public DbBuilder progressDialog(@Nullable ProgressBar progressDialog, @Nullable String message) {
        getParam().progressBar = progressDialog;
        getParam().progressMessage = message;
        return this;
    }

    /**
     * Show dialog builder.
     *
     * @param isDialog if true then display dialog while query executing
     * @return the builder
     */
    public DbBuilder showDialog(boolean isDialog) {
        getParam().isDialog = isDialog;
        return this;
    }

    /**
     * add query for read/fetch operation
     *
     * @param query {@link Query}
     * @return the builder
     */
    public DbBuilder query(Query query) {
        getParam().query = query;
        return this;
    }

    /**
     * finally execute query and get result either via callback
     *
     * @return the object either string/data model
     */
    public Object execute() {
        return param.queryType.execute(param);
    }

    /*******
     *Getters*
     * ****/
    public DbParam getParam() {
        return param;
    }
}
