package sparta.android.db;

import android.content.Context;
import android.widget.ProgressBar;

import io.realm.RealmModel;

/**
 * Created by android on 26/12/17.
 * <p>Contains the parameter related to db transactions</p>
 */

public class DbParam<T extends RealmModel> {
    /*class/application level context*/
    Context context;
    /*class that are being used as database table*/
    Class<?> dbClass;
    /*normal model class*/
    Class<?> model;
    /*Class that need to use as a db model for the purpose of insert/update/delete/read*/
    T queryDataObject;
    /*transaction type - default is fetch*/
    QueryType queryType = QueryType.GET;
    /*Query parameters*/
    Query query = new Query();
    /**
     * The progress dialog need to display
     */
    boolean isDialog = false;

    /**
     * The Progress dialog.
     */
    ProgressBar progressBar;

    /**
     * The callback object
     */
    DbCallbackHandler.OnDbCallback callback;

    /**
     * The Progress dialog message.
     */
    String progressMessage;

    /**
     * The task identification
     */
    int taskId = 0;


    /***********
     *Getters*
     * ********/
    public Context getContext() {
        return context;
    }

    public Class<?> getDbClass() {
        return dbClass;
    }

    public Class<?> getModel() {
        return model;
    }

    public boolean isDialog() {
        return isDialog;
    }

    public DbCallbackHandler.OnDbCallback getCallback() {
        return callback;
    }

    public int getTaskId() {
        return taskId;
    }

    public T getQueryDataObject() {
        return queryDataObject;
    }

    public Query getQuery() {
        return query;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
