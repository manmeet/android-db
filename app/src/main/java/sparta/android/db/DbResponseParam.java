package sparta.android.db;


/**
 * Created by android on 9/1/18.
 * <p>Used for the callback response based on database transaction.</p>
 *
 * @see DbCallbackHandler
 */

public class DbResponseParam<T> {
    /*Message after db transaction success or failure*/
    private String message;
    /*Task identifier that being performed for this data*/
    private int taskId;
    /*data based on db query*/
    private T response;

    /**
     * Constructor definition
     *
     * @param message  message for call back
     * @param response from database transaction
     */
    public DbResponseParam(String message, T response) {
        setMessage(message);
        setResponse(response);
    }


    /**
     * Constructor definition
     *
     * @param message  message for call back
     * @param response from database transaction
     * @param taskId   task identifier
     */
    public DbResponseParam(String message, T response, int taskId) {
        setMessage(message);
        setResponse(response);
        setTaskId(taskId);
    }


    /*********
     *Getters & Setters*
     ********/
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
