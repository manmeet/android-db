package sparta.android.db;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import sparta.android.db.realm.RealmManagerUtils;

/**
 * Created by android on 8/1/18.
 * <p>Contains builder to build configuration of database</p>
 */

public class DbConfiguration {
    /*Database schema version*/
    private static int sDbVersion = 1;
    /*database name*/
    private static String sDbName = "Db";
    /*Migration class object*/
    private static RealmMigration sMigrationClassObject;

    /**
     * private Constructor definition
     */
    private DbConfiguration() {
    }


    public static class Builder<T extends RealmMigration> {
        /*Instance variables*/
        private String dbName;
        private int dbVersion;
        private T migrationClassObject;

        /**
         * Database name
         *
         * @param name
         */
        public Builder databaseName(@NonNull String name) {
            this.dbName = name;
            return this;
        }

        /**
         * Database version
         *
         * @param version
         */
        public Builder databaseVersion(int version) {
            this.dbVersion = version;
            return this;
        }

        /**
         * Database migration
         *
         * @param migrationObject
         */
        public Builder migration(T migrationObject) {
            migrationClassObject = migrationObject;
            return this;
        }

        /**
         * Finally config values
         */
        public void build() {
            setDbName(dbName);
            setDbVersion(dbVersion);
            setMigrationClassObject(migrationClassObject);

           //set configuration
            RealmManagerUtils.configRealm();
        }

        /*********
         *Setters*
         * ******/
        private static synchronized void setDbVersion(int version) {
            sDbVersion = version;
        }

        private static synchronized void setDbName(@NonNull String name) {
            sDbName = name;
        }

        private static synchronized void setMigrationClassObject(RealmMigration migrationClassObject) {
            sMigrationClassObject = migrationClassObject;
        }
    }

    /**********
     *Getters*
     * *******/
    public static int getDbVersion() {
        return sDbVersion;
    }

    public static String getDbName() {
        return sDbName;
    }

    public static RealmMigration getMigrationClassObject() {
        return sMigrationClassObject;
    }
}
