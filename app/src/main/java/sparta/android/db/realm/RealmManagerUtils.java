package sparta.android.db.realm;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import sparta.android.db.DbConfiguration;
import sparta.android.db.DbParam;
import sparta.android.db.DbResponseParam;

/**
 * Created by android on 26/12/17.
 * <p>Contains the CRUD operation related to realm class</p>
 */

public class RealmManagerUtils {
    /*Logging tag for this class*/
    private static final String TAG = RealmManagerUtils.class.getSimpleName();
    /*Messages */
    private static final String ERROR_ALREADY_TRANSACTION_MSG = "Already in write transaction";
    private static final String INSERT_SUCCESS_MSG = "Insert successfully";
    private static final String INSERT_UPDATE_SUCCESS_MSG = "Insert or update successfully";
    private static final String DELETE_SUCCESS_MSG = "delete successfully";
    private static final String READ_SUCCESS_MSG = "Query successful";

    /**
     * Constructor definition
     * private constructor because of this members are global static
     */
    private RealmManagerUtils() {
    }


    /****************
     * Insertion*
     * *************/

    /**
     * To add new data row in ORM based database
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     */
    @SuppressWarnings("unchecked")
    public static void insert(@NonNull final DbParam param, @NonNull final Realm realmInstance) {
        Log.d(TAG, "insert()");
        if (!realmInstance.isInTransaction()) {
            realmInstance.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.insert(param.getQueryDataObject());

                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(INSERT_SUCCESS_MSG, null, param.getTaskId()));
                    }
                }
            });
        } else {
            Log.d(TAG, "insert() -> " + ERROR_ALREADY_TRANSACTION_MSG + " " + realmInstance);
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(ERROR_ALREADY_TRANSACTION_MSG, null, param.getTaskId()));
            }
        }
    }

    /**
     * To add new data row in ORM based database
     *
     * @param param {@link DbParam}
     */

    public static void insert(@NonNull DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            insert(param, realm);

            //close realm
            closeRealm(realm);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
    }

    /**
     * To add new data row in ORM based database asynchronously
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @param isCloseRealm  true if need to close object after transaction
     */
    @SuppressWarnings("unchecked")
    public static void insertAsync(@NonNull final DbParam param, @NonNull final Realm realmInstance,
                                   final boolean isCloseRealm) {
        Log.d(TAG, "insertAsync()");
        if (!realmInstance.isInTransaction()) {
            realmInstance.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.insert(param.getQueryDataObject());
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                    Log.d(TAG, "insertAsync() -> onSuccess ");
                    //check to close realm
                    if (isCloseRealm) {
                        closeRealm(realmInstance);
                    }
                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(INSERT_SUCCESS_MSG, null, param.getTaskId()));
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                    Log.d(TAG, "insertAsync() -> onError -> " + error.getMessage());
                    //check to close realm
                    if (isCloseRealm) {
                        closeRealm(realmInstance);
                    }
                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(error.getMessage(), null, param.getTaskId()));
                    }
                }
            });
        } else {
            Log.d(TAG, "insertAsync() -> " + ERROR_ALREADY_TRANSACTION_MSG + " " + realmInstance);
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(ERROR_ALREADY_TRANSACTION_MSG, null, param.getTaskId()));
            }
        }
    }

    /**
     * To add new data row in ORM based database
     *
     * @param param {@link DbParam}
     */

    public static void insertAsync(@NonNull DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            insertAsync(param, realm, true);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
    }


    /****************
     * Insertion OR update*
     * *************/

    /**
     * To update data in particular row if already available with same data
     * otherwise add new data row in ORM based database
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @param isCloseRealm  true if need to close object after transaction
     */
    @SuppressWarnings("unchecked")
    public static void insertOrUpdate(@NonNull final DbParam param, @NonNull final Realm realmInstance) {
        Log.d(TAG, "insertOrUpdate()");
        if (!realmInstance.isInTransaction()) {
            realmInstance.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.insertOrUpdate(param.getQueryDataObject());

                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(INSERT_UPDATE_SUCCESS_MSG, null, param.getTaskId()));
                    }
                }
            });
        } else {
            Log.d(TAG, "insertOrUpdate() -> " + ERROR_ALREADY_TRANSACTION_MSG + " " + realmInstance);
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(ERROR_ALREADY_TRANSACTION_MSG, null, param.getTaskId()));
            }
        }
    }

    /**
     * To update data in particular row if already available with same data
     * otherwise add new data row in ORM based database
     *
     * @param param {@link DbParam}
     */

    public static void insertOrUpdate(@NonNull DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            insertOrUpdate(param, realm);

            //close realm
            closeRealm(realm);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
    }

    /**
     * To update data in particular row if already available with same data
     * otherwise add new data row in ORM based database
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @param isCloseRealm  true if need to close object after transaction
     */
    @SuppressWarnings("unchecked")
    public static void insertOrUpdateAsync(@NonNull final DbParam param, @NonNull final Realm realmInstance,
                                           final boolean isCloseRealm) {
        Log.d(TAG, "insertOrUpdateAsync()");
        if (!realmInstance.isInTransaction()) {
            realmInstance.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.insertOrUpdate(param.getQueryDataObject());
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                    Log.d(TAG, "insertOrUpdateAsync() -> onSuccess ");

                    //check to close realm
                    if (isCloseRealm) {
                        closeRealm(realmInstance);
                    }

                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(INSERT_UPDATE_SUCCESS_MSG, null, param.getTaskId()));
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                    Log.d(TAG, "insertOrUpdateAsync() -> onError -> " + error.getMessage());

                    //check to close realm
                    if (isCloseRealm) {
                        closeRealm(realmInstance);
                    }

                    //check if callback is available then redirect url
                    if (param.getCallback() != null) {
                        param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(error.getMessage(), null, param.getTaskId()));
                    }
                }
            });
        } else {
            Log.d(TAG, "insertOrUpdateAsync() -> " + ERROR_ALREADY_TRANSACTION_MSG + " " + realmInstance);
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(ERROR_ALREADY_TRANSACTION_MSG, null, param.getTaskId()));
            }
        }
    }

    /**
     * To update data in particular row if already available with same data
     * otherwise add new data row in ORM based database
     *
     * @param param {@link DbParam}
     */

    public static void insertOrUpdateAsync(@NonNull DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            insertOrUpdateAsync(param, realm, true);

            //close realm
            closeRealm(realm);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
    }

    /****************
     * Deletion*
     * *************/

    /**
     * To delete data from database
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @param isCloseRealm  true if need to close object after transaction
     */
    @SuppressWarnings("unchecked")
    public static <T extends RealmModel> boolean delete(@NonNull final DbParam param, @NonNull final Realm realmInstance,
                                                        final boolean isCloseRealm) {
        Log.d(TAG, "delete()");
        boolean isDeleted = false;
        RealmResults<T> result = query(param, realmInstance);
        try {
            realmInstance.beginTransaction();
            if (result != null
                    && !result.isEmpty()) {

                result.deleteFirstFromRealm();
                realmInstance.commitTransaction();

                isDeleted = true;

                //check if callback is available then redirect url
                if (param.getCallback() != null) {
                    param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(DELETE_SUCCESS_MSG, true, param.getTaskId()));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            realmInstance.cancelTransaction();
            isDeleted = false;

            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(e.getMessage(), null, param.getTaskId()));
            }
        } finally {
            //check to close realm
            if (isCloseRealm) {
                closeRealm(realmInstance);
            }
        }
        return isDeleted;
    }

    /**
     * To delete data from database
     *
     * @param param {@link DbParam}
     */

    public static boolean delete(@NonNull final DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            return delete(param, realm, true);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
        return false;
    }

    /**
     * To delete all related data from database
     *
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @param isCloseRealm  true if need to close object after transaction
     */
    @SuppressWarnings("unchecked")
    public static <T extends RealmModel> boolean deleteAll(@NonNull final DbParam param, @NonNull final Realm realmInstance,
                                                           final boolean isCloseRealm) {
        Log.d(TAG, "deleteAll()");
        boolean isDeleted = false;
        RealmResults<T> result = query(param, realmInstance);
        try {
            realmInstance.beginTransaction();
            if (result != null
                    && !result.isEmpty()) {

                result.deleteAllFromRealm();
                realmInstance.commitTransaction();

                isDeleted = true;

                //check if callback is available then redirect url
                if (param.getCallback() != null) {
                    param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(DELETE_SUCCESS_MSG, true, param.getTaskId()));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            realmInstance.cancelTransaction();
            isDeleted = false;

            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(e.getMessage(), null, param.getTaskId()));
            }
        } finally {
            //check to close realm
            if (isCloseRealm) {
                closeRealm(realmInstance);
            }
        }
        return isDeleted;
    }

    /**
     * To delete all related data from database
     *
     * @param param {@link DbParam}
     */

    public static boolean deleteAll(@NonNull final DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            return deleteAll(param, realm, true);
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
        return false;
    }

    /*******************
     **Read/Fetch**
     * ****************/
    /**
     * Query from database based on request param.
     *
     * @param <E>   the type parameter based on object for list
     * @param param {@link DbParam}
     * @return List of the db class object type of T
     */
    public static <E extends RealmModel> List<E> query(@NonNull DbParam param) {
        try {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<E> result = query(param, realm);

            List<E> list = getResultData(realm, result);
            //finally close object if in open state
            closeRealm(realm);
            return list;
        }
        //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            //check if callback is available then redirect url
            if (param.getCallback() != null) {
                param.getCallback().onDbError(param.getQueryDataObject(), new DbResponseParam(eMigration.getMessage(), null, param.getTaskId()));
            }
        }
        return null;
    }

    /**
     * Query from database based on request param.
     *
     * @param <E>           the type parameter based on object
     * @param param         {@link DbParam}
     * @param realmInstance object of realm that is being handled to close on calling thread/activity/fragment
     * @return the db class object type of T
     */
    @SuppressWarnings("unchecked")
    public static <E extends RealmModel> RealmResults<E> query(@NonNull final DbParam param,
                                                               @NonNull final Realm realmInstance) {
        Log.d(TAG, "query()");

        RealmQuery realmQuery = realmInstance.where(param.getDbClass());
        //check if need equality
        validateEqualTo(realmQuery, param);
        //check if need not equality
        validateNotEqualTo(realmQuery, param);
        //check for between
        validateBetweenDate(realmQuery, param);
        //check for sorting
        validateSort(realmQuery, param);
        //check for limit
        validateDistinct(realmQuery, param);
        //check for greater than
        validateGreaterThan(realmQuery, param);
        //check for less than
        validateLessThan(realmQuery, param);
        //check for less than equal to
        validateLessThanEqualTo(realmQuery, param);
        //check for greater than equal to
        validateGreaterThanEqualTo(realmQuery, param);


        //finally call db
        RealmResults<E> result = realmQuery.findAll();
        Log.d(TAG, "query() -> result = " + result);

        //check if callback is available then redirect url
        if (param.getCallback() != null) {
            param.getCallback().onDbSuccess(param.getQueryDataObject(), new DbResponseParam(READ_SUCCESS_MSG, getResultData(realmInstance, result), param.getTaskId()));
        }

        return result;
    }


    /**
     * Functionality to check if where condition added for equal to value for eg - id = 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateEqualTo(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getEqualToKey())
                && !TextUtils.isEmpty(param.getQuery().getEqualToValue())) {
            realmQuery.equalTo(param.getQuery().getEqualToKey(), param.getQuery().getEqualToValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for not equal to value for eg - id = 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateNotEqualTo(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getNotEqualToKey())
                && !TextUtils.isEmpty(param.getQuery().getNotEqualToValue())) {
            realmQuery.notEqualTo(param.getQuery().getNotEqualToKey(), param.getQuery().getNotEqualToValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for greater than value for eg - number > 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateGreaterThan(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getGreaterThanKey())) {
            realmQuery.greaterThan(param.getQuery().getGreaterThanKey(), param.getQuery().getGreaterThanValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for greater than or equal to value for eg - number >= 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateGreaterThanEqualTo(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getGreaterThanOrEqualToKey())) {
            realmQuery.greaterThanOrEqualTo(param.getQuery().getGreaterThanOrEqualToKey(), param.getQuery().getGreaterThanOrEqualToValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for less than value for eg - number < 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateLessThan(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getLessThanKey())) {
            realmQuery.lessThan(param.getQuery().getLessThanKey(), param.getQuery().getLessThanValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for less than or equal to value for eg - number <= 1
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateLessThanEqualTo(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getLessThanOrEqualToKey())) {
            realmQuery.lessThanOrEqualTo(param.getQuery().getLessThanOrEqualToKey(), param.getQuery().getLessThanOrEqualToValue());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for distinct column value
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateDistinct(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getDistinct())) {
            realmQuery.distinctValues(param.getQuery().getDistinct());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for date between for eg - date between '2017-01-01' to '2017-02-01'
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateBetweenDate(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getBetweenDateKey())
                && param.getQuery().getBetweenDateFrom() != null
                && param.getQuery().getBetweenDateTo() != null) {
            realmQuery.between(param.getQuery().getBetweenDateKey(), param.getQuery().getBetweenDateFrom(), param.getQuery().getBetweenDateTo());
        }
        return realmQuery;
    }

    /**
     * Functionality to check if where condition added for orderby
     *
     * @param realmQuery {@link RealmQuery}
     * @param param      {@link DbParam}
     * @return modified realmQuery
     */
    private static <E extends RealmModel> RealmQuery<E> validateSort(@NonNull RealmQuery realmQuery, @NonNull DbParam param) {
        if (!TextUtils.isEmpty(param.getQuery().getDistinct())) {
            realmQuery.sort(param.getQuery().getOrderBy());
        }
        return realmQuery;
    }

    /**
     * Functionality to set realm configuration
     */
    public static void configRealm() {
        RealmConfiguration.Builder config = new RealmConfiguration.Builder();
        config.name(DbConfiguration.getDbName());
        config.schemaVersion(DbConfiguration.getDbVersion());
        //check for migration null
        if (DbConfiguration.getMigrationClassObject() != null) {
            config.migration(DbConfiguration.getMigrationClassObject());
        }
        //finally build configuration
        Realm.setDefaultConfiguration(config.build());
    }

    /**
     * Functionality to put realm response data into list
     *
     * @param <E>     the type parameter based on object for list
     * @param realm   {@link Realm}
     * @param results {@link RealmResults}
     * @return {@link List}
     */
    private static <E extends RealmModel> List<E> getResultData(Realm realm, RealmResults<E> results) {
        List<E> list = new ArrayList<>();
        if (results != null
                && !results.isEmpty()) {
            list = new ArrayList<>(realm.copyFromRealm(results));
        }
        return list;
    }

    /**
     * Functionality to get total count of realm object used Realm.getDefaultConfiguration()
     *
     * @return count of realm open object
     */
    public static int getOpenedRealmCount() {
        return Realm.getLocalInstanceCount(Realm.getDefaultConfiguration());
    }

    /**
     * Functionality to close realm object*
     *
     * @param realm {@link Realm}
     */
    public static void closeRealm(Realm realm) {
        Log.d(TAG, "closeRealm()");
        if (realm != null
                && !realm.isClosed()) {
            realm.close();
        }
        Log.d(TAG, "opened realm = " + getOpenedRealmCount());
    }

    /**
     * Functionality to get Realm object
     *
     * @return null or {@link Realm}
     */
    public static Realm getRealm() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
        } //To handle crash for migration
        catch (RealmMigrationNeededException eMigration) {
            Log.e(TAG, eMigration.getMessage());
        }
        return realm;
    }
}
