package sparta.android.db;

/**
 * Created by android on 26/12/17.
 * <p> Database transaction callbacks</p>
 */

public class DbCallbackHandler {

    /*Interface as a callback*/
    public interface OnDbCallback {
        /**
         * After success transaction from database
         *
         * @param dbModel  database ORM based model class
         * @param response {@link DbResponseParam}
         */
        <R extends DbResponseParam, M> void onDbSuccess(M dbModel, R response);

        /**
         * During transaction if there is an error
         *
         * @param dbModel  database ORM based model class
         * @param response {@link DbResponseParam}
         */
        <R extends DbResponseParam, M> void onDbError(M dbModel, R response);

        /**
         * During transaction interval till transaction complete
         *
         * @param taskId      task identifier that has been passed at the time of query
         * @param isCompleted true if transaction process is done else false
         */
        @SuppressWarnings("Not in use for Realm")
        void onDbProgress(int taskId, boolean isCompleted);
    }

    /*Class to handle callback*/
    public abstract class DbCallback implements OnDbCallback {
        @Override
        public void onDbSuccess(Object dbModel, DbResponseParam response) {

        }

        @Override
        public void onDbError(Object dbModel, DbResponseParam response) {

        }

        @Override
        public void onDbProgress(int taskId, boolean isCompleted) {

        }
    }
}
