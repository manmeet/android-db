package sparta.android.db;

import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by android on 5/1/18.
 * <p>To build query</p>
 */

public class QueryBuilder {
    /*Instance variables*/
    private Query queryParam;

    /**
     * Constructor definition
     */
    public QueryBuilder() {
        queryParam = new Query();
    }

    /**
     * where condition to check equality
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder equalTo(@NonNull String columnName, String columnValue) {
        getQueryParam().equalToKey = columnName;
        getQueryParam().equalToValue = columnValue;
        return this;
    }

    /**
     * where condition to check un-equality
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder notEqualTo(@NonNull String columnName, String columnValue) {
        getQueryParam().notEqualToKey = columnName;
        getQueryParam().notEqualToValue = columnValue;
        return this;
    }

    /**
     * where condition to check greater than value result
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder greaterThan(@NonNull String columnName, int columnValue) {
        getQueryParam().greaterThanKey = columnName;
        getQueryParam().greaterThanValue = columnValue;
        return this;
    }

    /**
     * where condition to check less than value result
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder lessThan(@NonNull String columnName, int columnValue) {
        getQueryParam().lessThanKey = columnName;
        getQueryParam().lessThanValue = columnValue;
        return this;
    }

    /**
     * where condition to check greater than or equal to value result
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder greaterThanOrEqualTo(@NonNull String columnName, int columnValue) {
        getQueryParam().greaterThanOrEqualToKey = columnName;
        getQueryParam().greaterThanOrEqualToValue = columnValue;
        return this;
    }

    /**
     * where condition to check less than or equal to value result
     *
     * @param columnName
     * @param columnValue
     * @return the builder
     */
    public QueryBuilder lessThanOrEqualTo(@NonNull String columnName, int columnValue) {
        getQueryParam().lessThanOrEqualToKey = columnName;
        getQueryParam().lessThanOrEqualToValue = columnValue;
        return this;
    }

    /**
     * where condition to find values between particular date
     *
     * @param columnName
     * @param dateFrom   start date
     * @param dateTo     end date
     * @return the builder
     */
    public QueryBuilder between(@NonNull String columnName, Date dateFrom, Date dateTo) {
        getQueryParam().betweenDateKey = columnName;
        getQueryParam().betweenDateFrom = dateFrom;
        getQueryParam().betweenDateTo = dateTo;
        return this;
    }

    /**
     * where condition to find distinct value
     *
     * @param distinctColumnName
     * @return the builder
     */
    public QueryBuilder distinct(@NonNull String distinctColumnName) {
        getQueryParam().distinct = distinctColumnName;
        return this;
    }

    /**
     * where condition to sort result based on column
     *
     * @param sortByColumn
     * @return the builder
     */
    public QueryBuilder sortBy(@NonNull String sortByColumn) {
        getQueryParam().orderBy = sortByColumn;
        return this;
    }

    /**
     * where condition to group result based on column
     *
     * @param groupByColumn
     * @return the builder
     */
    public QueryBuilder groupBy(@NonNull String groupByColumn) {
        getQueryParam().grpBy = groupByColumn;
        return this;
    }

    /**
     * where condition to having value
     *
     * @param havingValue
     * @return the builder
     */
    public QueryBuilder having(@NonNull String havingValue) {
        getQueryParam().having = havingValue;
        return this;
    }

    /**
     * where condition to find result count limit
     *
     * @param limitCount
     * @return the builder
     */
    @SuppressWarnings("Limit is not working for Realm database")
    public QueryBuilder limit(@NonNull int limitCount) {
        getQueryParam().limit = limitCount;
        return this;
    }


    /**
     * Finally return the prepared query and its parameters
     *
     * @return {@link Query}
     */
    public Query prepare() {
        return getQueryParam();
    }

    /*********
     *Getters*
     * ******/
    public Query getQueryParam() {
        return queryParam;
    }
}
