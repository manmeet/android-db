## Android Local Database Realm Library  ##

[![](https://jitpack.io/v/org.bitbucket.manmeet/android-db.svg)](https://jitpack.io/#org.bitbucket.manmeet/android-db)


#### Dependency ####
* minSdkVersion 15
* targetSdkVersion 25
* Realm 4.3.1
* Gson 2.8.2
* [BeanUtils](http://commons.apache.org/proper/commons-beanutils/)

#### How to add in your Android application ####
Step-I - Add it in your root build.gradle at the end of repositories:
```gradle
      	allprojects {
      		repositories {
      			...
      			maven { url 'https://jitpack.io' }
      		}
      	}      	 
```
Step-II - Add the dependency in your app build.gradle
```gradle
        dependencies {
            ...
	        compile 'org.bitbucket.manmeet:android-db:0.1.7'
	    }
```


#### Example code ####
* To read data from database
```java 
    DbConnect.operation(this, QueryType.GET, Angel.class, null)
                    .callback(new DbCallbackHandler.OnDbCallback() {
                        @Override
                        public void onDbSuccess(Object dbModel, DbResponseParam response) {
                        }
    
                        @Override
                        public void onDbError(Object dbModel, DbResponseParam response) {
                        }
    
                        @Override
                        public void onDbProgress(int taskId, boolean isCompleted) {
                        }
                    })
                    .successModel(AngelDataModel.class)
                    .execute();
```

* To insert data into database
```java 
    angel = new Angel();
    angel.setName("name: " + new Random().nextInt());
    DbConnect.operation(this, QueryType.INSERT_ASYNC, Angel.class, angel)
                    .callback(new DbCallbackHandler().new DbCallback() {
                        @Override
                        public void onDbSuccess(Object dbModel, DbResponseParam response) {
                            super.onDbSuccess(dbModel, response);
                        }
                    })
                    .execute();
```


#### Database configuration ####
Add following in your Application class
```java
    public class DbApplication extends Application {
       @Override
       public void onCreate() {
           super.onCreate();

           Realm.init(this);
           new DbConfiguration.Builder()
                .databaseName("test-db")
                .databaseVersion(1)
                .build();
    }
}
```


#### Callbacks ####
```java 
implements DbCallbackHandler.OnDbCallback
```

* `onDbSuccess()`
* `onDbError()`
* `onDbProgress()` 


#### Queries/Transactions support ####
* Insert - Sync & Async both
* InsertOrUpdate - Sync & Async both
* Delete - Sync only
* DeleteAll - - Sync only
* Read - - Sync only

#### Response/Error codes ####
* 101 - Realm object is already in trasanction so this transaction query can not executed. You can use ASYNC instead
* 102 - Migration is required. You must add migration class in configuration

#### Note: ####
* Simultaneously multiple database requests works with asynchronous requests
* Progress dialog feature is not available with this version of library. You can add your own progress dialog on your fragment/activity
* `onDbProgress()` is not supported with this version of library
* Read query is working on main thread and the response may slow if added `.outputModel()` for big results
* BeanUtils library's Android [version](http://www.java2s.com/Code/Jar/a/Downloadandroidjavaairbridgejar.htm) is used to copy realm model class data to normal model class data  
